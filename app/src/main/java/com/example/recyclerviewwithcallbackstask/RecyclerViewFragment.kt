package com.example.recyclerviewwithcallbackstask

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.example.recyclerviewwithcallbackstask.databinding.FragmentRecyclerViewBinding
import com.example.recyclerviewwithcallbackstask.model.*

class RecyclerViewFragment : Fragment(), OnItemClickListener {

    private var _binding: FragmentRecyclerViewBinding? = null
    private val binding get() = _binding!!

    private val listOfAnimals = listOf(
        Cat("cat Murchik", false),
        Dog("dog Bobik", false),
        Tiger("Kisa tiger", false),
        Crow("crow CarCarich", true),
        Rooster("Common petuh", false),
        Cat("cat Garfild", false),
        Crow("White crow", true),
        Tiger("Simbo", false),
        Rooster("Your best friend", false),
        Dog("dog Sharik", false)
        )

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentRecyclerViewBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val adapter = AnimalsAdapter()

        adapter.setCallback(this)
        adapter.submitList(listOfAnimals)

        binding.apply {
            recyclerview.adapter = adapter
            recyclerview.itemAnimator = null
        }
        super.onViewCreated(view, savedInstanceState)
    }

    override fun onVoiceButtonClick(animal: Animal) {
        Toast.makeText(context, animal.voice(), Toast.LENGTH_LONG).show()
    }
}