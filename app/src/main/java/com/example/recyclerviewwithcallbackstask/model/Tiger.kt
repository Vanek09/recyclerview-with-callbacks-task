package com.example.recyclerviewwithcallbackstask.model

data class Tiger(override val name: String, override val canFly: Boolean): Animal(name, canFly) {
    override fun voice() = "RRRRRRRRR"
}