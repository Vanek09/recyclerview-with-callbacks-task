package com.example.recyclerviewwithcallbackstask.model

data class Dog(override val name: String, override val canFly: Boolean): Animal(name, canFly) {
    override fun voice() = "WOF WOF WOF"
}