package com.example.recyclerviewwithcallbackstask.model

data class Crow(override val name: String, override val canFly: Boolean): Animal(name, canFly) {
    override fun voice() = "Caw, Caw, Caw"
}