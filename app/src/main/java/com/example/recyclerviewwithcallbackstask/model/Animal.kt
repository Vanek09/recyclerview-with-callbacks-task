package com.example.recyclerviewwithcallbackstask.model

abstract class Animal(open val name: String, open val canFly: Boolean) {
    abstract fun voice(): String
}