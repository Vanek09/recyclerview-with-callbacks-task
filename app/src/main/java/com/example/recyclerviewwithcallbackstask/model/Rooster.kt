package com.example.recyclerviewwithcallbackstask.model

data class Rooster(override val name: String, override val canFly: Boolean): Animal(name, canFly) {
    override fun voice() = "Crow-Crow"
}