package com.example.recyclerviewwithcallbackstask.model

data class Cat(override val name: String, override val canFly: Boolean): Animal(name, canFly) {
    override fun voice() = "Meow... Meow..."
}