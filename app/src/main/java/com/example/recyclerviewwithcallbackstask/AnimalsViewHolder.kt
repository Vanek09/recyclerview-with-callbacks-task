package com.example.recyclerviewwithcallbackstask

import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import com.example.recyclerviewwithcallbackstask.databinding.*
import com.example.recyclerviewwithcallbackstask.model.Animal

class AnimalsViewHolder<T : ViewDataBinding>(
    private val binding: T,
    animals: List<Animal>,
    private val listener: OnItemClickListener?
) : RecyclerView.ViewHolder(binding.root) {

    init {
        when (binding) {
            is CatListItemBinding -> {
                binding.voiceButton.setOnClickListener {
                    val position = adapterPosition
                    if (position != RecyclerView.NO_POSITION) {
                        val animal = animals[position]
                        listener?.onVoiceButtonClick(animal)
                    }
                }
            }

            is CrowListItemBinding -> {
                binding.voiceButton.setOnClickListener {
                    val position = adapterPosition
                    if (position != RecyclerView.NO_POSITION) {
                        val animal = animals[position]
                        listener?.onVoiceButtonClick(animal)
                    }
                }
            }

            is DogListItemBinding -> {
                binding.voiceButton.setOnClickListener {
                    val position = adapterPosition
                    if (position != RecyclerView.NO_POSITION) {
                        val animal = animals[position]
                        listener?.onVoiceButtonClick(animal)
                    }
                }
            }

            is RoosterListItemBinding -> {
                binding.voiceButton.setOnClickListener {
                    val position = adapterPosition
                    if (position != RecyclerView.NO_POSITION) {
                        val animal = animals[position]
                        listener?.onVoiceButtonClick(animal)
                    }
                }
            }

            is TigerListItemBinding -> {
                binding.voiceButton.setOnClickListener {
                    val position = adapterPosition
                    if (position != RecyclerView.NO_POSITION) {
                        val animal = animals[position]
                        listener?.onVoiceButtonClick(animal)
                    }
                }
            }
        }
    }

    fun bind(animal: Animal) {
        when (binding) {
            is CatListItemBinding -> {
                binding.name = animal.name
                binding.fly = if (animal.canFly) CAN_FLY else CAN_NOT_FLY
            }

            is CrowListItemBinding -> {
                binding.name = animal.name
                binding.fly = if (animal.canFly) CAN_FLY else CAN_NOT_FLY
            }

            is DogListItemBinding -> {
                binding.name = animal.name
                binding.fly = if (animal.canFly) CAN_FLY else CAN_NOT_FLY
            }

            is RoosterListItemBinding -> {
                binding.name = animal.name
                binding.fly = if (animal.canFly) CAN_FLY else CAN_NOT_FLY
            }

            is TigerListItemBinding -> {
                binding.name = animal.name
                binding.fly = if (animal.canFly) CAN_FLY else CAN_NOT_FLY
            }
        }
    }

    private companion object {
        const val CAN_FLY = "Can Fly"
        const val CAN_NOT_FLY = "Can`t Fly"
    }
}