package com.example.recyclerviewwithcallbackstask

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.recyclerviewwithcallbackstask.databinding.*
import com.example.recyclerviewwithcallbackstask.model.*

class AnimalsAdapter: RecyclerView.Adapter<AnimalsViewHolder<ViewDataBinding>>() {

    private val differCallback = object : DiffUtil.ItemCallback<Animal>() {
        override fun areItemsTheSame(oldItem: Animal, newItem: Animal): Boolean {
            return oldItem === newItem
        }

        override fun areContentsTheSame(oldItem: Animal, newItem: Animal): Boolean {
            return oldItem.name == newItem.name &&
                    oldItem.canFly == newItem.canFly
        }
    }

    private val animals = AsyncListDiffer(this, differCallback)

    private var listener: OnItemClickListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AnimalsViewHolder<ViewDataBinding> = when(viewType) {

        R.layout.cat_list_item -> AnimalsViewHolder(
                CatListItemBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                ),
                animals.currentList,
                listener
            )

        R.layout.crow_list_item -> AnimalsViewHolder(
            CrowListItemBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            ),
            animals.currentList,
            listener
        )

        R.layout.dog_list_item -> AnimalsViewHolder(
            DogListItemBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            ),
            animals.currentList,
            listener
        )

        R.layout.rooster_list_item -> AnimalsViewHolder(
            RoosterListItemBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            ),
            animals.currentList,
            listener
        )

        R.layout.tiger_list_item -> AnimalsViewHolder(
            TigerListItemBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            ),
            animals.currentList,
            listener
        )

        else -> throw IllegalArgumentException("Invalid viewType provided")
    }

    override fun onBindViewHolder(holder: AnimalsViewHolder<ViewDataBinding>, position: Int) {
        holder.bind(animals.currentList[position])
    }

    override fun getItemCount(): Int = animals.currentList.size

    override fun getItemViewType(position: Int): Int = when(animals.currentList[position]) {
        is Cat -> R.layout.cat_list_item
        is Crow -> R.layout.crow_list_item
        is Dog -> R.layout.dog_list_item
        is Rooster -> R.layout.rooster_list_item
        is Tiger -> R.layout.tiger_list_item
        else -> throw IllegalArgumentException("It`s not animal")
    }

    fun submitList(list: List<Animal>) {
        animals.submitList(list)
    }

    fun setCallback(callback: OnItemClickListener) {
        listener = callback
    }
}