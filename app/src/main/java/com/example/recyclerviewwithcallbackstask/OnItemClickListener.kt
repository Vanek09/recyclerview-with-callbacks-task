package com.example.recyclerviewwithcallbackstask

import com.example.recyclerviewwithcallbackstask.model.Animal

interface OnItemClickListener {
    fun onVoiceButtonClick(animal: Animal)
}